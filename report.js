const { getTrips, getDriver, getVehicle } = require("api");
function convertToNumber(numberString) {
  return Number(parseFloat(String(numberString).replace(",", "")).toFixed(2));
}
/**
 * This function should return the trip data analysis
 *
 * Question 3
 * @returns {any} Trip data analysis
 */
async function analysis() {
  // Your code goes here
  let dataContainer = {
    noOfCashTrips: 0,
    noOfNonCashTrips: 0,
    billedTotal: 0,
    cashBilledTotal: 0,
    nonCashBilledTotal: 0,
    noOfDriversWithMoreThanOneVehicle: 0,
    mostTripsByDriver: {},
    highestEarningDriver: {},
  };

  let sortedByID = {};
  let tripsByDriver = {};
  let earnedByDriver = {};
  let driverVehicleNUmbersData = {};

  const tripsData = await getTrips();
  let IdOfAllDrivers = [...new Set(tripsData.map((trip) => trip.driverID))];

  let driverIDMap = IdOfAllDrivers.map(async (driver) => {
    try {
      sortedByID[driver] = await getDriver(driver);
    } catch (error) {}
  });
  await Promise.all(driverIDMap);

  for (let trip of tripsData) {
    const getTrip = () => {
      amount = trip.billedAmount;
      billedAmount = convertToNumber(amount);
      dataContainer.billedTotal += billedAmount;
      if (trip.isCash == true) {
        dataContainer.cashBilledTotal += billedAmount;
        dataContainer.noOfCashTrips++;
      } else {
        dataContainer.nonCashBilledTotal += billedAmount;
        dataContainer.noOfNonCashTrips++;
      }
    };
    getTrip();

    let IDofEachDriver = trip.driverID;
    const getTripByDriver = () => {
      if (!driverVehicleNUmbersData.hasOwnProperty(IDofEachDriver)) {
        try {
          let driver = sortedByID[IDofEachDriver];
          driverVehicleNUmbersData[IDofEachDriver] = driver.vehicleID.length;
        } catch (error) {}
      }
      if (tripsByDriver.hasOwnProperty(IDofEachDriver)) {
        tripsByDriver[IDofEachDriver]++;
      } else {
        tripsByDriver[trip.driverID] = 1;
      }
    };
    getTripByDriver();

    const getMostTripDriverID = () => {
      const mostTripsDriverID = Object.keys(tripsByDriver).reduce((a, b) =>
        tripsByDriver[a] < tripsByDriver[b] ? b : a
      );
      return mostTripsDriverID;
    };
    getMostTripDriverID();

    if (earnedByDriver.hasOwnProperty(IDofEachDriver)) {
      earnedByDriver[IDofEachDriver] += Number(
        parseFloat(String(trip.billedAmount).replace(",", "")).toFixed(2)
      );
    } else {
      earnedByDriver[IDofEachDriver] = Number(
        parseFloat(String(trip.billedAmount).replace(",", "")).toFixed(2)
      );
    }
  }

  let driverVehicleArray = Object.keys(driverVehicleNUmbersData);

  getDataContainer = () => {
    for (let driver of driverVehicleArray) {
      if (driverVehicleNUmbersData[driver] > 1) {
        dataContainer.noOfDriversWithMoreThanOneVehicle++;
      }
    }
    dataContainer.billedTotal = Number(dataContainer.billedTotal.toFixed(2));
    dataContainer.cashBilledTotal = Number(
      dataContainer.cashBilledTotal.toFixed(2)
    );
    dataContainer.nonCashBilledTotal = Number(
      dataContainer.nonCashBilledTotal.toFixed(2)
    );

    return dataContainer;
  };
}

analysis();
module.exports = analysis;
