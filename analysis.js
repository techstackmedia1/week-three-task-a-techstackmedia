const { getTrips, getDriver } = require("api");
/**
 * This function should return the trip data analysis
 *
 * Question 3
 * @returns {any} Trip data analysis
 */
async function analysis() {
  // Your code goes here
  let getOutput = {
    noOfCashTrips: 0,
    noOfNonCashTrips: 0,
    billedTotal: 0,
    cashBilledTotal: 0,
    nonCashBilledTotal: 0,
    noOfDriversWithMoreThanOneVehicle: 0,
    mostTripsByDriver: {
      name: "Driver name",
      email: "Driver email",
      phone: "Driver phone",
      noOfTrips: 0,
      totalAmountEarned: 0,
    },
    highestEarningDriver: {
      name: "Driver name",
      email: "Driver email",
      phone: "Driver phone",
      noOfTrips: 0,
      totalAmountEarned: 0,
    },
  };

  let driverIDPropertyMap = {}; // an object to store driverID mapped to their properties
  let tripsByDriver = {}; // object to store number of trips by each driver
  let earnedByDriver = {};
  let driverVehicleNUmbersData = {}; //object to store driverID mapped to their number of vehicles
  const tripData = await getTrips();

  let allDriversID = [...new Set(tripData.map((trip) => trip.driverID))];

  let driverIDMap = allDriversID.map(async (driver) => {
    try {
      driverIDPropertyMap[driver] = await getDriver(driver);
    } catch (error) {
      console.log("error");
    }
  });

  await Promise.all(driverIDMap);

  for (let trip of tripData) {
    const getBilledAmount = () => {
      let billedAmount = Number(
        Number(String(trip.billedAmount).replace(",", "")).toFixed(2)
      );
      getOutput.billedTotal += billedAmount;
      if (trip.isCash == true) {
        getOutput.cashBilledTotal += billedAmount;
        getOutput.noOfCashTrips++;
      } else {
        getOutput.nonCashBilledTotal += billedAmount;
        getOutput.noOfNonCashTrips++;
      }
    };
    getBilledAmount();

    let currentDriverID = trip.driverID;
    if (!driverVehicleNUmbersData.hasOwnProperty(currentDriverID)) {
      try {
        let driver = driverIDPropertyMap[currentDriverID];
        driverVehicleNUmbersData[currentDriverID] = driver.vehicleID.length;
      } catch (error) {}
    }

    if (tripsByDriver.hasOwnProperty(currentDriverID)) {
      tripsByDriver[currentDriverID]++;
    } else {
      tripsByDriver[trip.driverID] = 1;
    }

    let mostTripsDriverID = Object.keys(tripsByDriver).reduce((a, b) =>
      tripsByDriver[a] < tripsByDriver[b] ? b : a
    );
    const getMostTripsDriver = (mostTripsDriver) => {
      getOutput.mostTripsByDriver.name = mostTripsDriver.name;
      getOutput.mostTripsByDriver.email = mostTripsDriver.email;
      getOutput.mostTripsByDriver.phone = mostTripsDriver.phone;
      getOutput.mostTripsByDriver.noOfTrips = tripsByDriver[mostTripsDriverID];
      getOutput.mostTripsByDriver.totalAmountEarned =
        earnedByDriver[mostTripsDriverID];
      if (earnedByDriver.hasOwnProperty(currentDriverID)) {
        earnedByDriver[currentDriverID] += Number(
          Number(String(trip.billedAmount).replace(",", "")).toFixed(2)
        );
      } else {
        earnedByDriver[currentDriverID] = Number(
          Number(String(trip.billedAmount).replace(",", "")).toFixed(2)
        );
      }
    };
    getMostTripsDriver(driverIDPropertyMap[mostTripsDriverID]);

    const mostEarnedDriverID = Object.keys(earnedByDriver).reduce((a, b) =>
      earnedByDriver[a] < earnedByDriver[b] ? b : a
    );

    const getEarnedByDriver = (mostEarnedDriver) => {
      getOutput.highestEarningDriver.name = mostEarnedDriver.name;
      getOutput.highestEarningDriver.email = mostEarnedDriver.email;
      getOutput.highestEarningDriver.phone = mostEarnedDriver.phone;
      getOutput.highestEarningDriver.noOfTrips =
        tripsByDriver[mostEarnedDriverID];
      getOutput.highestEarningDriver.totalAmountEarned =
        earnedByDriver[mostEarnedDriverID];
    };
    getEarnedByDriver(driverIDPropertyMap[mostEarnedDriverID]);
  }

  let driverVehicleArray = Object.keys(driverVehicleNUmbersData);

  for (let driver of driverVehicleArray) {
    if (driverVehicleNUmbersData[driver] > 1) {
      getOutput.noOfDriversWithMoreThanOneVehicle++;
    }
  }

  getOutput.billedTotal = Number(getOutput.billedTotal.toFixed(2));
  getOutput.cashBilledTotal = Number(getOutput.cashBilledTotal.toFixed(2));
  getOutput.nonCashBilledTotal = Number(
    getOutput.nonCashBilledTotal.toFixed(2)
  );

  return getOutput;
}

module.exports = analysis;
